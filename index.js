const app = require('express')
const bodyParser = require('body-parser')
app.use(bodyParser.json())

const escpos = require('escpos')
escpos.USB = require('escpos-usb')
const device = new escpos.USB();
const printer = new escpos.Printer(device, { encoding: "GB18030" })

app.post('/print', (req, res) => {

    printAntrianAdmisi({antrian: '1A001'})

    res.json(
      { status: 'success' }
    )
  });

  function printAntrianAdmisi(data){
    device.open(function(error){
        printer
        .font('a')
        .align('ct')
        .style('bu')
        .size(1, 1)
        .text(data.antrian)
        .text('The quick brown fox jumps over the lazy dog')
        .text('The quick brown fox jumps over the lazy dog')
        .barcode('1234567', 'EAN8')
        .table(["One", "Two", "Three"])
        .tableCustom(
          [
            { text:"Left", align:"LEFT", width:0.33, style: 'B' },
            { text:"Center", align:"CENTER", width:0.33},
            { text:"Right", align:"RIGHT", width:0.33 }
          ],
          { encoding: 'cp857', size: [1, 1] } // Optional
        )
        .qrimage('https://github.com/song940/node-escpos', function(err){
          this.cut();
          this.close();
        });
      });
  }

app.listen(4000, () => {
    console.log(`<============ service printer berjalan diport 3000 ==========>`)
    console.log(`📎 doc http://localhost:4000/doc`)
});